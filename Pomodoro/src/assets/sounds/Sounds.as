package assets.sounds {
	
	import flash.media.Sound;
	
	public class Sounds {
		
		[Embed(source="finish.mp3")]
		private static var FinishEmbed:Class
		
		public static const finish:Sound = new FinishEmbed;
		
	}
	
}
package assets.icons {
	
	import flash.display.Bitmap;
	
	public class Icons {
		
		[Embed(source="pomodoro.png")]
		private static var PomodoroEmbed:Class
		
		public static const pomodoro:Bitmap = new PomodoroEmbed;
		
	}
	
}